# Parent Repo for Screening Projects

## Bootstrap Website Project (Section A : Making a simple website with vanilla JS)

> [![Live Link](https://img.shields.io/badge/REPO-LINK-green)](https://gitlab.com/next-lab-projects/bootstrap-simple-website)

---

## Lazy Loading Project (Section B: Lazy Loading To Avoid Pagination (vanilla JS))

> [![Live Link](https://img.shields.io/badge/REPO-LINK-green)](https://gitlab.com/next-lab-projects/lazy-loading-project)

---

## Assessment Video Link 

> [![Live Link](https://img.shields.io/badge/REPO-LINK-green)](https://drive.google.com/file/d/1Lm0Pz4Fkfyx5aexm1KiI5GYInyUAW18Y/view?usp=sharing)

---
